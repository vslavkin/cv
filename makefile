Document = CV
Language = english
Portfolio = false
LATEX = lualatex
Output = $(Document)-$(Language)

export Document
export Portfolio
export Language
export Output

.PHONY: all spanish english spanish-portfolio english-portfolio clean

# %: Document = $@
# %: build

all : spanish english spanish-portfolio english-portfolio

spanish-portfolio:
	$(MAKE) -k Portfolio=true Language=spanish Output=$(Document)-Portfolio-spanish build

english-portfolio:
	$(MAKE) -k Portfolio=true Language=english Output=$(Document)-Portfolio-english build

spanish:
	$(MAKE) -k build Language=spanish
english:
	$(MAKE) -k build Language=english

build:
	$(LATEX) -jobname=$(Output) -synctex=1 -interaction=nonstopmode '\def\portfolio{$(Portfolio)} \def\doclanguage{$(Language)} \input{$(Document)}'

clean:
	rm -f *.aux *.out *.log

# latexmk -pdf -pdflatex='lualatex -jobname=CV-spanish "\\def\\portfolio{false} \\def\\doclanguage{spanish} \\input{CV}"'
